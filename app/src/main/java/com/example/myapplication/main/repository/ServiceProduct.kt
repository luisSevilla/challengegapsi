package com.example.myapplication.main.repository

import com.example.myapplication.main.data.ProductResponse
import retrofit2.Call
import retrofit2.http.*

interface ServiceProduct {

    @Headers("X-IBM-Client-Id: adb8204d-d574-4394-8c1a-53226a40876e")
    @GET("demo-gapsi/search?")
    fun getProduct( @Query("query") query: String, @Query("page") id: String): Call<ProductResponse>

}