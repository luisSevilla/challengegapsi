package com.example.myapplication.main.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class ProductModel (
    @SerializedName("productId")
    var productId: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("imageUrl")
    var imageUrl: String? = null,

    @SerializedName("primaryOffer")
    var primaryOffer: @RawValue primaryOff? = null
        ) : Parcelable


@Parcelize
data class primaryOff(
    @SerializedName("offerId")
    var offerId: String? = null,

    @SerializedName("offerPrice")
    var offerPrice: String? = null,

    @SerializedName("listPrice")
    var listPrice: String? = null,

    @SerializedName("currencyCode")
    var currencyCode: String? = null
        ) : Parcelable