package com.example.myapplication.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.main.data.ProductModel
import com.example.myapplication.main.data.ProductResponse
import com.example.myapplication.main.repository.ServiceProduct
import com.example.myapplication.products.ContainerActivity
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {
    var call: Call<ProductResponse>? = null
    var senProduct: ArrayList<ProductModel>? = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_search.setOnClickListener {
            if (edt_product.text.isNullOrEmpty()){
                Toast.makeText(this,"Ingrese Producto a buscar", Toast.LENGTH_SHORT).show()

            }else{
                progressBarlayout.visibility = View.VISIBLE
                getlogin(edt_product.text.toString(),"1")
            }

        }
    }

    private fun getRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .client(client)
            .baseUrl("https://00672285.us-south.apigw.appdomain.cloud/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getlogin(query: String, id: String){
        call = getRetrofit().create(ServiceProduct::class.java).getProduct(query, id)
        call?.enqueue(object : Callback<ProductResponse?> {
            override fun onResponse(call: Call<ProductResponse?>, response: Response<ProductResponse?>) {
                val loginResponse = response.body()
                if (loginResponse?.responseStatus.isNullOrEmpty()){
                    progressBarlayout.visibility = View.GONE
                    Toast.makeText(this@MainActivity,"Fail",Toast.LENGTH_SHORT).show()
                }else {
                    for (item in loginResponse?.productDetails!!) {
                        senProduct?.add(item)
                    }

                    progressBarlayout.visibility = View.GONE
                    val mIntent = Intent(this@MainActivity, ContainerActivity::class.java)
                    mIntent.putParcelableArrayListExtra("PRODUCTS", senProduct)
                    startActivity(mIntent)
                }
            }

            override fun onFailure(call: Call<ProductResponse?>, t: Throwable) {
                progressBarlayout.visibility = View.GONE
                Toast.makeText(this@MainActivity,"Fail" + t.message,Toast.LENGTH_SHORT).show()

            }
        })
    }
}