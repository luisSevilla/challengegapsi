package com.example.myapplication.main.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class ProductResponse (
    @SerializedName("responseStatus")
    var responseStatus: String? = null,

    @SerializedName("responseMessage")
    var responseMessage: String? = null,

    @SerializedName("sortStrategy")
    var sortStrategy: String? = null,

    @SerializedName("domainCode")
    var domainCode: String? = null,

    @SerializedName("keyword")
    var keyword: String? = null,

    @SerializedName("numberOfProducts")
    var numberOfProducts: String? = null,

    @SerializedName("productDetails")
    var productDetails: List<ProductModel>? = null
        ) : Parcelable
