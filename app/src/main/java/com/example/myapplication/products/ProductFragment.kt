package com.example.myapplication.products

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.main.data.ProductModel
import com.example.myapplication.products.Adapter.ProductAdapter
import com.example.myapplication.products.history.MyHistoryProvider
import kotlinx.android.synthetic.main.fragment_product.*


class ProductFragment: Fragment() {
    private var allProducts = ArrayList<ProductModel>()

    companion object{
        val TAG: String = "RechargeFragment"

        fun newInstance(products: ArrayList<ProductModel>): ProductFragment {
            val fragment = ProductFragment()
            val args = Bundle()
            args.putParcelableArrayList("PRODUCTS", products)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root: View = inflater.inflate(R.layout.fragment_product, container, false)
        allProducts = requireArguments().getParcelableArrayList<ProductModel>("PRODUCTS") as ArrayList<ProductModel>
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_list_products.setHasFixedSize(false)
        val mLayoutManager = LinearLayoutManager(requireContext())
        rv_list_products.layoutManager = mLayoutManager

        rv_list_products.adapter = ProductAdapter(requireActivity(), allProducts)

    }

}