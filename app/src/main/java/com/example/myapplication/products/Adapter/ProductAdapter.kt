package com.example.myapplication.products.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.main.data.ProductModel
import kotlin.collections.ArrayList

class ProductAdapter (var context: Context?, var listProduct: ArrayList<ProductModel>): RecyclerView.Adapter<ProductAdapter.ViewHolder>(){


    inner class  ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTittle : TextView = itemView.findViewById(R.id.txt_tittle)
        var txtPrice : TextView = itemView.findViewById(R.id.txt_price)
        var img : ImageView = itemView.findViewById(R.id.img_product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent,false))
    }

    override fun getItemCount(): Int {
        return listProduct.size
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        holder.txtTittle.text = listProduct[i].title
        holder.txtPrice.text = listProduct[i].primaryOffer?.listPrice
        Glide.with(context!!).load(listProduct[i].imageUrl).into(holder.img)
    }

}