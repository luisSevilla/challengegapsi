package com.example.myapplication.products.history

import android.content.SearchRecentSuggestionsProvider

class MyHistoryProvider: SearchRecentSuggestionsProvider() {
    companion object {
        val AUTHORITY = "com.example.myapplication.products.history.MyHistoryProvider"
        val MODE = SearchRecentSuggestionsProvider.DATABASE_MODE_QUERIES
    }

    init {
        setupSuggestions(AUTHORITY, MODE)
    }
}