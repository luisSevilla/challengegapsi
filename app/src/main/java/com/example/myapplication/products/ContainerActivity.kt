package com.example.myapplication.products

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.cursoradapter.widget.CursorAdapter
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.main.data.ProductModel
import com.example.myapplication.products.Adapter.ProductAdapter
import com.example.myapplication.products.history.MyHistoryProvider
import kotlinx.android.synthetic.main.fragment_product.*

class ContainerActivity: AppCompatActivity() {
    private var allProducts = ArrayList<ProductModel>()
    private var suggestions: SearchRecentSuggestions? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_product)

        val bundle = intent.extras
        allProducts = bundle?.getParcelableArrayList<ProductModel>("PRODUCTS")!!

        //Log.d("productsActivity", prod.toString())

        /*supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameLayout, ProductFragment.newInstance(prod as ArrayList<ProductModel>))
            .commit()*/

        rv_list_products.setHasFixedSize(false)
        val mLayoutManager = LinearLayoutManager(this)
        rv_list_products.layoutManager = mLayoutManager

        rv_list_products.adapter = ProductAdapter(this, allProducts)
        suggestions = SearchRecentSuggestions(
            this,
            MyHistoryProvider.AUTHORITY, MyHistoryProvider.MODE
        )

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        product_search.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        product_search.isQueryRefinementEnabled = true
        val columNames = arrayOf(SearchManager.SUGGEST_COLUMN_TEXT_1)
        val viewIds = intArrayOf(android.R.id.text1)
        val adapter: CursorAdapter = SimpleCursorAdapter(
            this,
            android.R.layout.simple_list_item_1,
            null,
            columNames,
            viewIds
        )
        product_search.suggestionsAdapter = adapter
        product_search.requestFocus(1);
        product_search.setOnQueryTextListener(object: SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                val suggestions = SearchRecentSuggestions(
                   this@ContainerActivity,
                    MyHistoryProvider.AUTHORITY,
                    MyHistoryProvider.MODE
                )
                suggestions.saveRecentQuery(query, null)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return true
            }

        })

        product_search.setOnSuggestionListener(object : SearchView.OnSuggestionListener,
            androidx.appcompat.widget.SearchView.OnSuggestionListener {
            override fun onSuggestionSelect(position: Int): Boolean {
                return true
            }

            override fun onSuggestionClick(position: Int): Boolean {
                val selectedView: androidx.cursoradapter.widget.CursorAdapter? = product_search.getSuggestionsAdapter()
                val cursor: Cursor = selectedView?.getItem(position) as Cursor
                val index: Int = cursor.getColumnIndexOrThrow(SearchManager.SUGGEST_COLUMN_TEXT_1)
                product_search.setQuery(cursor.getString(index), true)
                return true
            }
        })

        val intent = intent

        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            val suggestions = SearchRecentSuggestions(
                this,
                MyHistoryProvider.AUTHORITY, MyHistoryProvider.MODE
            )
            suggestions.saveRecentQuery(query, null)
        }


    }
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {

        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            //use the query to search your data somehow
        }
    }


}